import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";

import { router } from "routes";

import { CalculatorContextProvider } from "context/calculatorContext";

import "normalize.css";
import "styles/index.scss";

const root = ReactDOM.createRoot(
	document.getElementById("root") as HTMLDivElement
);

root.render(
	<React.StrictMode>
		<CalculatorContextProvider>
			<RouterProvider router={router} />
		</CalculatorContextProvider>
	</React.StrictMode>
);
