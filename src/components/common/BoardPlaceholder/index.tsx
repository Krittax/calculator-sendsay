import { ReactComponent as Pic } from "images/pic.svg";

import styles from "./styles.module.scss";

function BoardPlaceholder() {
	return (
		<div className={styles.boardPlaceholder}>
			<div className={styles.placeholderInfo}>
				<Pic />
				<h2>Перетащите сюда</h2>
				<span>любой элемент из левой панели</span>
			</div>
		</div>
	);
}

export { BoardPlaceholder };
