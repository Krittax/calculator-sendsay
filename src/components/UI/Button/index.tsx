import cn from "classnames";

import styles from "./styles.module.scss";

interface ButtonProps {
	id: string;
	value: string;
	isActive: boolean;
	onClick: (value: string) => void;
	extraGridConfig: {
		id: string;
		columnStart: number;
		columnEnd: number;
	}[];
}

function Button({ extraGridConfig, id, value, isActive, onClick }: ButtonProps) {
	const gridConfig = extraGridConfig.find((config) => config.id === id);

	let gridStyles = {};

	if (gridConfig) {
		gridStyles = {
			gridColumnStart: gridConfig.columnStart,
			gridColumnEnd: gridConfig.columnEnd,
		};
	}

	const clickHandler = () => {
		onClick(id);
	};

	return (
		<button
			className={cn(styles.btn, isActive && styles.active)}
			style={{
				...gridStyles,
			}}
			onClick={clickHandler}
		>
			{value}
		</button>
	);
}

export { Button };
