import { Display } from "features/display";
import { OperandList } from "features/operands";
import { OperatorList } from "features/operators";
import { Equals } from "features/equals";

export const DIV_BY_ZERO_MESSAGE = "Не определено";
export const MAX_VALUE_LENGTH = 8;

export const BOARDS_DATA = [
	{
		id: "parts",
		sortable: false,
		items: [
			{
				id: "display",
				order: 0,
				component: Display,
				state: "active",
				draggableOnce: true,
				draggable: true,
				fixedOrder: true,
			},
			{
				id: "operators",
				order: 1,
				component: OperatorList,
				state: "active",
				draggableOnce: false,
				draggable: true,
				fixedOrder: false,
			},
			{
				id: "operands",
				order: 2,
				component: OperandList,
				state: "active",
				draggableOnce: false,
				draggable: true,
				fixedOrder: false,
			},
			{
				id: "equal",
				order: 3,
				component: Equals,
				state: "active",
				draggableOnce: false,
				draggable: true,
				fixedOrder: false,
			},
		],
	},
	{ id: "canvas", sortable: true, items: [] },
]

export enum ActionKind {
	addValue = "ADD_VALUE",
	addOperator = "ADD_OPERATOR",
	finishOperation = "FINISH_OPERATION",
	resetState = "RESET_STATE",
};