import { createContext, useReducer } from "react";

import { reducer } from "context/calculatorContext/reducer";
import {
	addOperatorAction,
	addValueAction,
	finishOperationAciton,
	resetStateAction,
} from "context/calculatorContext/actions";

import {
	CalculatorProviderProps,
	InitContext,
	CalculatorState,
} from "context/calculatorContext/types";

const initContext: InitContext = {
	value: "0",
	result: null,
	operator: null,
	operatorSelected: false,
	operatorsActive: true,
	afterEqual: false,
	addValue: (number: string) => {},
	addOperator: (operator: string) => {},
	finishOperation: () => {},
	resetState: () => {}
};

export const initCalculatorState: CalculatorState = {
	value: "0",
	result: null,
	operatorSelected: false,
	operator: null,
	operatorsActive: true,
	afterEqual: false,
};

const calculatorContext = createContext(initContext);

function CalculatorContextProvider({ children }: CalculatorProviderProps) {
	const [state, dispatch] = useReducer(reducer, initCalculatorState);

	const {
		value,
		result,
		operator,
		operatorSelected,
		operatorsActive,
		afterEqual,
	} = state;

	const addValueHandler = (number: string) => {
		dispatch( addValueAction(number) );
	};

	const addOperatorHandler = (operator: string) => {
		dispatch( addOperatorAction(operator) );
	};

	const finishOperationHandler = () => {
		dispatch( finishOperationAciton() );
	};

	const resetStateHandler = () => {
		dispatch( resetStateAction() );
	};

	return (
		<calculatorContext.Provider
			value={{
				value,
				result,
				operator,
				operatorSelected,
				operatorsActive,
				afterEqual,
				addValue: addValueHandler,
				addOperator: addOperatorHandler,
				finishOperation: finishOperationHandler,
				resetState: resetStateHandler
			}}
		>
			{children}
		</calculatorContext.Provider>
	);
}

export { calculatorContext, CalculatorContextProvider };
