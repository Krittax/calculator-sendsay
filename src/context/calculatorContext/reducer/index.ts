import { ActionKind, DIV_BY_ZERO_MESSAGE, MAX_VALUE_LENGTH } from "data/constants";
import { getResult } from "utils/calculatorContext";

import { initCalculatorState } from "..";

import { CalculatorState, Action } from "../types";

export function reducer(
	state: CalculatorState,
	action: Action
): CalculatorState {
	if (action.type === ActionKind.addValue) {
		if (typeof action.payload !== "string") {
			return state;
		}

		const isCommaEntered = action.payload.includes(",");
		const isCommaExist = state.value.includes(",");

		if (isCommaEntered && state.operatorSelected) {
			return {
				...state,
				value: "0" + action.payload,
				operatorSelected: false,
			};
		}

		if (isCommaEntered && isCommaExist) {
			return state;
		}

		const maxLengthReached = (state.value + action.payload).length >= MAX_VALUE_LENGTH;

		if (isCommaExist && !state.operatorSelected) {
			return {
				...state,
				value: maxLengthReached ? action.payload : state.value + action.payload,
				operatorSelected: false,
			};
		}

		if (state.operatorSelected || !state.operatorsActive) {
			return {
				...state,
				value: isCommaEntered ? "0" + action.payload : action.payload,
				operatorSelected: false,
				operatorsActive: true,
				afterEqual: false,
			};
		}

		let newValue = maxLengthReached ? state.value : state.value + action.payload;

		if ((state.value === "0" && !isCommaEntered) || state.afterEqual) {
			newValue = isCommaEntered ? state.value + action.payload : action.payload;
		}

		return { ...state, value: newValue, afterEqual: false };
	}

	if (action.type === ActionKind.addOperator) {
		if (typeof action.payload !== "string") {
			return state;
		}
		const [commaValue, value] = getResult(state, state.operator);

		if (action.payload !== state.operator && state.operatorSelected) {
			return {
				...state,
				operator: action.payload,
			};
		}

		if (state.operatorSelected) {
			return {
				...state,
				operator: action.payload,
			};
		}

		if (commaValue === DIV_BY_ZERO_MESSAGE) {
			return {
				...state,
				value: commaValue,
				result: value,
				operatorsActive: false,
				operator: "+",
			};
		}

		return {
			...state,
			operator: action.payload,
			value: commaValue,
			result: value,
			operatorSelected: true,
		};
	}

	if (action.type === ActionKind.finishOperation) {
		const [commaValue, value] = getResult(state, state.operator);

		if (state.operatorSelected) {
			return state;
		}

		if (commaValue === DIV_BY_ZERO_MESSAGE) {
			return {
				...state,
				value: commaValue,
				result: value,
				operatorsActive: false,
				operator: "+",
				afterEqual: true,
			};
		}

		return {
			...state,
			value: commaValue,
			result: 0,
			operatorSelected: false,
			operator: "+",
			afterEqual: true,
		};
	}

	if (action.type === ActionKind.resetState) {
		return initCalculatorState;
	}

	return state;
}