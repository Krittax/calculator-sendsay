import { ActionKind } from "data/constants"; 

import { ActionCreator } from "../types";

export const addValueAction: ActionCreator = (payload) => {
	return { type: ActionKind.addValue, payload };
};

export const addOperatorAction: ActionCreator = (payload) => {
	return { type: ActionKind.addOperator, payload }
};

export const finishOperationAciton: ActionCreator = () => {
	return { type: ActionKind.finishOperation };
};

export const resetStateAction: ActionCreator = () => {
	return { type: ActionKind.resetState }
};
