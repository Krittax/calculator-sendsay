import { OmitFunc } from "utils/generics";
import { ActionKind } from "data/constants";

export interface CalculatorProviderProps {
	children: React.ReactNode;
}

export interface InitContext {
	value: string;
	result: number | null;
	operator: string | null;
	operatorSelected: boolean;
	operatorsActive: boolean,
	afterEqual: boolean
	addValue: (number: string) => void;
	addOperator: (operator: string) => void;
	finishOperation: () => void;
	resetState: () => void;
}

export type CalculatorState = OmitFunc<InitContext>;

export type Action = {
	type: ActionKind,
	payload?: number | string
};

export type ActionCreator = (payload?: number | string) => Action