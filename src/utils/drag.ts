import { ComponentType } from "features/boards/types";

export const sortByOrder = () => {
	return (a: ComponentType, b: ComponentType) => {
		if (a.order > b.order) {
			return 1;
		} else {
			return -1;
		}
	};
};
