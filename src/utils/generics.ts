type NonFunctionPropNames<T extends {}> = {
	[P in keyof T]: T[P] extends Function ? never : P
}[keyof T];

export type OmitFunc<T extends {}> = Pick<T, NonFunctionPropNames<T>>;