import Decimal from "decimal.js";

export const operateMethods = {
	"+": (a: number, b: number) => +new Decimal(a).plus(b),
	"-": (a: number, b: number) => +new Decimal(a).minus(b),
	"*": (a: number, b: number) => +new Decimal(a).times(b),
	"/": (a: number, b: number) => +new Decimal(a).div(b)
};