import { operateMethods } from "./methods";
import { DIV_BY_ZERO_MESSAGE, MAX_VALUE_LENGTH } from "data/constants";

import { isAnObjectPropertyPredicate } from "./predicates";
import { CalculatorState } from "context/calculatorContext/types";

export function getResult(state: CalculatorState, operator: string | null) {
	const validFormattedValue = state.value.replace(/,/g, ".");

	let operateMethod: null | ((a: number, b: number) => number) = null;

	if (operator && isAnObjectPropertyPredicate(operateMethods, operator)) {
		operateMethod = operateMethods[operator];
	}

	if (operateMethod && typeof state.result === "number") {
		const newResult = operateMethod(state.result, Number(validFormattedValue));
		let commaFormattedValue = String(newResult).replace(/\./g, ",");

		if (commaFormattedValue.length > MAX_VALUE_LENGTH) {
			commaFormattedValue = commaFormattedValue.slice(0, MAX_VALUE_LENGTH);
		}

		if (!isFinite(newResult)) {
			return [DIV_BY_ZERO_MESSAGE, 0] as const;
		}

		return [commaFormattedValue, newResult] as const;
	}

	return [state.value, +state.value.replace(/,/g, ".")] as const;
};
