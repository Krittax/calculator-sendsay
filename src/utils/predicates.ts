export function isAnObjectPropertyPredicate<T extends {}>(obj: T, prop: PropertyKey): prop is keyof typeof obj {
	return prop in obj;
}
