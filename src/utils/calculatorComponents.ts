export function getCursorType(isRuntime: boolean, isDraggable: boolean) {
	let cursorType = isRuntime ? "pointer" : "move";

	if (!isDraggable) {
		cursorType = "no-drop";
	}

	return cursorType;
}