import { useContext } from "react";

import cn from "classnames";

import { calculatorContext } from "context/calculatorContext";

import { getCursorType } from "utils/calculatorComponents";

import { CalculatorComponentProps } from "features/types";

import styles from "./styles.module.scss"

function Equals({ runtimeActive, draggble }: CalculatorComponentProps) {
	const { finishOperation } = useContext(calculatorContext);

	const cursorType = getCursorType(runtimeActive, draggble);

	const clickEqualHandler = () => {
		if (runtimeActive) {
			finishOperation();
		}
	};

	return (
		<div className={cn("calc-component", runtimeActive && "no-selection")} style={{cursor: cursorType}}>
			<button className={styles.equals} style={{ cursor: "inherit" }} onClick={clickEqualHandler}>
				=
			</button>
		</div>
	);
}

export { Equals };