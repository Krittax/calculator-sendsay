import { useContext } from "react";

import cn from "classnames";

import { calculatorContext } from "context/calculatorContext";

import { GridList } from "features/grid-list";

import { getCursorType } from "utils/calculatorComponents";

import OPERANDS_DATA from "data/operands.json";

import { CalculatorComponentProps } from "features/types";

const gridConfig = [{ id: "0", columnStart: 1, columnEnd: 3 }];

function OperandList({ runtimeActive, draggble }: CalculatorComponentProps) {
	const { addValue } = useContext(calculatorContext);

	const cursorType = getCursorType(runtimeActive, draggble);

	const clickOperandHandler = (number: string) => {
		if (runtimeActive) {
			addValue(number);
		}
	};

	return (
		<div
			className={cn("calc-component", runtimeActive && "no-selection")}
			style={{
				cursor: cursorType,
			}}
		>
			<GridList
				data={OPERANDS_DATA}
				columns="repeat(3, 1fr)"
				extraGridConfig={gridConfig}
				buttonsActive={runtimeActive}
				onClick={clickOperandHandler}
			/>
		</div>
	);
}

export { OperandList };
