import { useContext } from "react";

import cn from "classnames";

import { GridList } from "features/grid-list";

import { calculatorContext } from "context/calculatorContext";

import { getCursorType } from "utils/calculatorComponents";

import OPERATOR_DATA from "data/operators.json";

import { CalculatorComponentProps } from "features/types";

function OperatorList({ runtimeActive, draggble }: CalculatorComponentProps) {
	const { addOperator, operatorsActive } = useContext(calculatorContext);

	let cursorType = getCursorType(runtimeActive, draggble);

	if (!operatorsActive) {
		cursorType = "no-drop";
	}

	const clickOperatorHandler = (id: string) => {
		if (runtimeActive) {
			addOperator(id);
		}
	};

	return (
		<div
			className={cn("calc-component", !operatorsActive && "disabled", runtimeActive && "no-selection")}
			style={{ cursor: cursorType }}
		>
			<GridList
				data={OPERATOR_DATA}
				columns="repeat(4, 1fr)"
				extraGridConfig={[]}
				buttonsActive={runtimeActive && operatorsActive}
				onClick={clickOperatorHandler}
			/>
		</div>
	);
}

export { OperatorList };
