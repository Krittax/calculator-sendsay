export interface ModeSwitchProps {
	runtimeActive: boolean;
	onToggleMode: (modeType: "runtime" | "constructor") => void;
}