import { useContext } from "react";

import cn from "classnames";

import { calculatorContext } from "context/calculatorContext";

import { ReactComponent as Eye } from "images/eye.svg";
import { ReactComponent as Selector } from "images/selector.svg";

import { ModeSwitchProps } from "features/mode-switch/types";

import styles from "./styles.module.scss";

function ModeSwitch({ onToggleMode, runtimeActive }: ModeSwitchProps) {
	const { resetState } = useContext(calculatorContext);
	
	return (
		<div className={styles.switcher}>
			<button
				className={cn(styles.switchBtn, runtimeActive && styles.active)}
				onClick={() => onToggleMode("runtime")}
			>
				<Eye />
				Runtime
			</button>
			<button
				className={cn(styles.switchBtn, !runtimeActive && styles.active)}
				onClick={() => {
					onToggleMode("constructor");
					resetState();
				}}
			>
				<Selector />
				Constructor
			</button>
		</div>
	);
}

export { ModeSwitch };
