import { PointerLineProps } from "features/pointer-line/types";

import styles from "./styles.module.scss";

function PointerLine({ positionStyles }: PointerLineProps) {
	return (
		<div
			className={styles.line}
			style={{
				...positionStyles,
			}}
		></div>
	);
}

export { PointerLine };
