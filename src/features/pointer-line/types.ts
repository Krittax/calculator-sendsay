export interface PointerLineProps {
	positionStyles: Record<string, string>;
}