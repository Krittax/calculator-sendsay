import { useContext } from "react";

import cn from "classnames";

import { calculatorContext } from "context/calculatorContext"

import { DIV_BY_ZERO_MESSAGE } from "data/constants";
import { getCursorType } from "utils/calculatorComponents";

import { CalculatorComponentProps } from "features/types";

import styles from "./styles.module.scss";

function Display({ runtimeActive, draggble }: CalculatorComponentProps) {
	const calculatorCtx = useContext(calculatorContext);

	const isOperationError = calculatorCtx.value === DIV_BY_ZERO_MESSAGE;

	let cursorType = getCursorType(runtimeActive, draggble);

	if (cursorType === "no-drop" && runtimeActive) {
		cursorType = "default";
	}

	return (
		<div className="calc-component" style={{ cursor: cursorType }}>
			<div className={cn(styles.display, isOperationError && styles.errorMessage)}>
				{calculatorCtx.value}
			</div>
		</div>
	);
}

export { Display };