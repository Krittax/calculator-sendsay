import { useState } from "react";

import { sortByOrder } from "utils/drag";

import { BoardsType, BoardType, ComponentType } from "../types";

function useDrag(initBoardsData: BoardsType) {
	const [boards, setBoards] = useState(initBoardsData);

	const [activeComponent, setActiveComponent] = useState<ComponentType>();
	const [activeBoard, setActiveBoard] = useState<BoardType>();

	const [dragOverBoard, setDragOverBoard] = useState<BoardType | null>(null);
	const [dragOverComponent, setDragOverComponent] =
		useState<ComponentType | null>(null);

	const [runtimeActive, setRuntimeActive] = useState(false);

	const dragStartHandler = (
		event: React.DragEvent<HTMLDivElement>,
		board: BoardType,
		component: ComponentType
	) => {
		if (component.state === "disabled" || !component.draggable) {
			event.preventDefault();
			return;
		}

		setActiveComponent(component);
		setActiveBoard(board);
	};

	const dragLeaveHandler = () => {
		setDragOverBoard(null);
		setDragOverComponent(null);
	};

	const dragOverHandler = (
		event: React.DragEvent<HTMLDivElement>,
		board: BoardType,
		component: ComponentType | null
	) => {
		event.preventDefault();
		event.stopPropagation();

		if (!board.sortable || component?.fixedOrder) {
			return;
		}

		setDragOverBoard(board);
		setDragOverComponent(component);
	};

	const dragEndHandler = () => {
		setDragOverBoard(null);
		setDragOverComponent(null);
	};

	const dropHandler = (
		event: React.DragEvent<HTMLDivElement>,
		board: BoardType,
		component: ComponentType | null
	) => {
		event.preventDefault();
		event.stopPropagation();

		if (!board.sortable) {
			return;
		}

		if (!activeBoard || !activeComponent) {
			return;
		}

		const originalBoardList = activeBoard.items.slice();

		if (activeBoard.id === board.id) {
			let orderIndex = 0;

			const newPlaceIndex = board.items.findIndex((item) => {
				if (component?.fixedOrder && component === item) {
					return true;
				}
				return item.id === component?.id;
			});

			if (newPlaceIndex !== -1 && originalBoardList[newPlaceIndex].fixedOrder) {
				return;
			}

			const oldPlaceIndex = board.items.findIndex(
				(item) => item.id === activeComponent.id
			);

			const newOrderNum = originalBoardList[newPlaceIndex]
				? originalBoardList[newPlaceIndex].order
				: board.items.length - 1;
			const oldOrderNum = originalBoardList[oldPlaceIndex].order;

			if (newPlaceIndex !== -1) {
				originalBoardList[newPlaceIndex].order = oldOrderNum;
			} else {
				originalBoardList.forEach((item, index) => {
					if (oldPlaceIndex !== index) {
						item.order = orderIndex;
						orderIndex++;
					}
				});
			}

			originalBoardList[oldPlaceIndex].order = newOrderNum;

			originalBoardList.sort(sortByOrder());

			setBoards((prevBoardsState) => {
				return prevBoardsState.map((board) => {
					if (board.id === activeBoard.id) {
						return { ...board, items: originalBoardList };
					}
					return board;
				});
			});
			return;
		}

		if (component?.fixedOrder) {
			return;
		}

		const targetBoardList = board.items.slice();

		const draggableComponentIndex = activeBoard.items.findIndex(
			(item) => item.id === activeComponent.id
		);

		const newIndex = board.items.findIndex((item) => item.id === component?.id);
		const componentCopy = {
			...activeComponent,
			draggable: activeComponent.draggableOnce ? false : true,
		};

		if (activeComponent.fixedOrder) {
			targetBoardList.splice(activeComponent.order, 0, componentCopy);
		} else if (newIndex === -1) {
			targetBoardList.push(componentCopy);
		} else {
			targetBoardList.splice(newIndex, 0, componentCopy);
		}

		targetBoardList.map((item, index) => (item.order = index));

		originalBoardList[draggableComponentIndex].state = "disabled";

		setBoards((prevBoardsState) =>
			prevBoardsState.map((b) => {
				if (b.id === activeBoard.id) {
					return { ...b, items: originalBoardList };
				}
				if (b.id === board.id) {
					return { ...b, items: targetBoardList };
				}
				return b;
			})
		);
	};

	const removeItemOnCanvasHandler = (
		board: BoardType,
		component: ComponentType
	) => {
		if (runtimeActive) {
			return;
		}

		const componentId = component.id;
		const boardId = board.id;

		if (!board.sortable) {
			return;
		}

		setBoards((prevBoardsStat) => {
			return prevBoardsStat.map((b) => {
				const componentIndex = b.items.findIndex(
					(item) => item.id === componentId
				);
				if (componentIndex === -1) {
					return b;
				}
				if (b.id === boardId) {
					b.items.splice(componentIndex, 1);
					return b;
				}

				const inactiveComponent = b.items[componentIndex];
				inactiveComponent.state = "active";
				inactiveComponent.draggable = true;

				return b;
			});
		});
	};

	const toggleModeHandler = (mode: "runtime" | "constructor") => {
		if (mode === "runtime") {
			setRuntimeActive(true);
			return;
		}

		setRuntimeActive(false);
	};

	return {
		boards,
		activeComponent,
		dragOverBoard,
		dragOverComponent,
		runtimeActive,
		dragStartHandler,
		dragLeaveHandler,
		dragOverHandler,
		dragEndHandler,
		dropHandler,
		removeItemOnCanvasHandler,
		toggleModeHandler
	};
}

export { useDrag };