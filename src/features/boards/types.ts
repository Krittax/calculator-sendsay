import { BOARDS_DATA } from "data/constants";

export type BoardsType = typeof BOARDS_DATA;

export type BoardType = BoardsType[number];
export type ComponentType = BoardType["items"][number];

type DragHandler = (
	event: React.DragEvent<HTMLDivElement>,
	board: BoardType,
	component: ComponentType | null
) => void;

export interface BoardProps {
	runtimeActive: boolean;
	board: BoardType;
	dragOverBoard: BoardType | null;
	dragOverComponent: ComponentType | null;
	activeComponent: ComponentType | undefined;
	dragOverHandler: DragHandler;
	dragLeaveHandler: () => void;
	dropHandler: DragHandler;

	dragStartHandler: (
		event: React.DragEvent<HTMLDivElement>,
		board: BoardType,
		component: ComponentType
	) => void;
	dragEndHandler: () => void;
	removeItemOnCanvasHandler: (
		board: BoardType,
		component: ComponentType
	) => void;
}

export interface BoardComponentProps {
	item: ComponentType;
	index: number;
	runtimeActive: boolean;
	board: BoardType;
	dragOverBoard: BoardType | null;
	dragOverComponent: ComponentType | null;
	activeComponent: ComponentType | undefined;
	dragStartHandler: (
		event: React.DragEvent<HTMLDivElement>,
		board: BoardType,
		component: ComponentType
	) => void;
	dragLeaveHandler: () => void;
	dragOverHandler: DragHandler;
	dropHandler: DragHandler;
	dragEndHandler: () => void;
	removeItemOnCanvasHandler: (
		board: BoardType,
		component: ComponentType
	) => void;
}
