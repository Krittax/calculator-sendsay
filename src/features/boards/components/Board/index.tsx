import cn from "classnames";

import { BoardComponent } from "../BoardComponent";
import { BoardPlaceholder } from "components/common/BoardPlaceholder";

import styles from "./styles.module.scss";

import { BoardProps } from "features/boards/types";

function Board({
	board,
	runtimeActive,
	dragOverBoard,
	activeComponent,
	dragOverComponent,
	dragLeaveHandler,
	dragOverHandler,
	dropHandler,
	dragEndHandler,
	dragStartHandler,
	removeItemOnCanvasHandler,
}: BoardProps) {
	const isBoardEmpty = board.items.length === 0;

	if (runtimeActive && board.id === "parts") {
		return <></>;
	}

	return (
		<div
			className={cn(
				styles.board,
				isBoardEmpty && dragOverBoard?.id === board.id && styles.empty,
				runtimeActive && styles.runtimeActive
			)}
			key={board.id}
			onDragOver={(event) => dragOverHandler(event, board, null)}
			onDragLeave={dragLeaveHandler}
			onDrop={(event) => dropHandler(event, board, null)}
		>
			{!isBoardEmpty &&
				board.items.map((item, index) => (
					<BoardComponent
						key={item.id}
						item={item}
						index={index}
						board={board}
						activeComponent={activeComponent}
						dragOverBoard={dragOverBoard}
						dragOverComponent={dragOverComponent}
						runtimeActive={runtimeActive}
						dragEndHandler={dragEndHandler}
						dragLeaveHandler={dragLeaveHandler}
						dragOverHandler={dragOverHandler}
						dragStartHandler={dragStartHandler}
						dropHandler={dropHandler}
						removeItemOnCanvasHandler={removeItemOnCanvasHandler}
					/>
				))}
			{isBoardEmpty && <BoardPlaceholder />}
		</div>
	);
}

export { Board };
