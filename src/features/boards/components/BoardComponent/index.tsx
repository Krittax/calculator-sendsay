import cn from "classnames";

import { PointerLine } from "features/pointer-line";

import { BoardComponentProps } from "features/boards/types";

import styles from "./styles.module.scss";

function BoardComponent({
	item,
	index,
	runtimeActive,
	board,
	activeComponent,
	dragOverComponent,
	dragOverBoard,
	dragEndHandler,
	dragLeaveHandler,
	dragOverHandler,
	dragStartHandler,
	dropHandler,
	removeItemOnCanvasHandler
}: BoardComponentProps) {
	const isDisabled = item.state === "disabled";
	const isFixedOrder = activeComponent?.fixedOrder;

	return (
		<div
			className={cn(styles.componentWrap, isDisabled && styles.disabled)}
			draggable={!runtimeActive}
			onDragStart={(event) => dragStartHandler(event, board, item)}
			onDragLeave={dragLeaveHandler}
			onDragOver={(event) => dragOverHandler(event, board, item)}
			onDrop={(event) => dropHandler(event, board, item)}
			onDragEnd={dragEndHandler}
			onDoubleClick={() => removeItemOnCanvasHandler(board, item)}
		>
			{!isFixedOrder &&
				dragOverComponent?.id === item.id &&
				!dragOverComponent.fixedOrder &&
				board.sortable && (
					<PointerLine
						positionStyles={{
							top: "0px",
						}}
					/>
				)}
			{(dragOverComponent || dragOverBoard) &&
				isFixedOrder &&
				activeComponent.order === index &&
				board.sortable && (
					<PointerLine
						positionStyles={{
							top: "0px",
						}}
					/>
				)}
			{!isFixedOrder &&
				!dragOverComponent &&
				dragOverBoard &&
				index === board.items.length - 1 &&
				board.sortable && (
					<PointerLine
						positionStyles={{
							bottom: "-6px",
						}}
					/>
				)}
			{
				<item.component
					runtimeActive={runtimeActive}
					draggble={item.draggable}
				/>
			}
		</div>
	);
}

export { BoardComponent };
