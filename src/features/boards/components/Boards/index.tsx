// import { useState } from "react";
import { useDrag } from "features/boards/hooks/useDrag";

import { Board } from "../Board";

import { ModeSwitch } from "features/mode-switch";
// import { PointerLine } from "features/pointer-line";
// import { BoardPlaceholder } from "components/common/BoardPlaceholder";

import { BOARDS_DATA } from "data/constants";

// import { BoardsProps } from "features/boards/types";

import styles from "./styles.module.scss";

function Boards() {
	const {
		activeComponent,
		boards,
		dragOverBoard,
		dragOverComponent,
		runtimeActive,
		dragEndHandler,
		dragLeaveHandler,
		dragOverHandler,
		dragStartHandler,
		dropHandler,
		removeItemOnCanvasHandler,
		toggleModeHandler,
	} = useDrag(BOARDS_DATA);

	return (
		<div className={styles.boardsWrap}>
			<>
				<ModeSwitch
					onToggleMode={toggleModeHandler}
					runtimeActive={runtimeActive}
				/>
				{boards.map((board) => {
					return (
						<Board
							key={board.id}
							runtimeActive={runtimeActive}
							board={board}
							dragOverBoard={dragOverBoard}
							activeComponent={activeComponent}
							dragOverComponent={dragOverComponent}
							dragLeaveHandler={dragLeaveHandler}
							dropHandler={dropHandler}
							dragOverHandler={dragOverHandler}
							dragStartHandler={dragStartHandler}
							dragEndHandler={dragEndHandler}
							removeItemOnCanvasHandler={removeItemOnCanvasHandler}
						/>
					);
				})}
			</>
		</div>
	);
}

export { Boards };
