export interface GridListProps {
	data: { id: string; value: string }[];
	columns: string;
	buttonsActive: boolean;
	onClick: (id: string) => void;
	extraGridConfig: {
		id: string;
		columnStart: number;
		columnEnd: number;
	}[];
}