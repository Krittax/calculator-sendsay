import { Button } from "components/UI/Button";

import { GridListProps } from "features/grid-list/types";

import styles from "./styles.module.scss";

function GridList({
	data,
	columns,
	extraGridConfig,
	buttonsActive,
	onClick,
}: GridListProps) {
	return (
		<div className={styles.gridList} style={{ gridTemplateColumns: columns }}>
			{data.map((item) => {
				return (
					<Button
						key={item.id}
						extraGridConfig={extraGridConfig}
						id={item.id}
						value={item.value}
						isActive={buttonsActive}
						onClick={onClick}
					/>
				);
			})}
		</div>
	);
}

export { GridList };
